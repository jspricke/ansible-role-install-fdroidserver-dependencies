#! /bin/bash

set -ex

dpkg -l vagrant > /dev/null

cd $(dirname $0)/role-test-vm

#vagrant destroy -f
#vagrant up
vagrant ssh -c 'cd /vagrant; ansible-playbook \
	--connection=local --inventory localhost, --limit localhost \
        --verbose \
        tests/playbook.yml'
