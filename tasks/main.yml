---

- name: 'check whether {{ ansible_distribution_release }}-backports are already configured'
  shell: "grep -r --include '*.list' '^[^#]* {{ ansible_distribution_release }}-backports ' /etc/apt"
  register: backports_grep
  ignore_errors: true
  when: ansible_distribution == 'Debian'
- name: "apt_repository: enable {{ ansible_distribution_release }}-backports"
  apt_repository:
    repo: 'deb https://deb.debian.org/debian {{ ansible_distribution_release }}-backports main'
    state: present
    update_cache: no
  when: ansible_distribution == 'Debian' and backports_grep.rc != 0

- name: "copy: apt pinning rule for stretch-backports packages"
  copy:
    content: |
      Package: androguard python3-asn1crypto python3-mwclient
      Pin: release a=stretch-backports
      Pin-Priority: 500
    dest: /etc/apt/preferences.d/debian-stretch-backports.pref
    mode: 0644
    owner: root
    group: root
  when: ansible_distribution == 'Debian' and ansible_distribution_release == 'stretch'

- name: "copy: apt pinning rule for buster-backports packages"
  copy:
    content: |
      Package: apksigner libapksig-java s3cmd
      Pin: release a=buster-backports
      Pin-Priority: 500
    dest: /etc/apt/preferences.d/debian-buster-backports.pref
    mode: 0644
    owner: root
    group: root
  when: ansible_distribution == 'Debian' and ansible_distribution_release == 'buster'

- name: "copy: apt pinning rule for bullseye-backports packages"
  copy:
    content: |
      Package: apksigner libapksig-java
      Pin: release a=bullseye-backports
      Pin-Priority: 500
    dest: /etc/apt/preferences.d/debian-bullseye-backports.pref
    mode: 0644
    owner: root
    group: root
  when: ansible_distribution == 'Debian' and ansible_distribution_release == 'bullseye'

- name: 'apt: update cache'
  apt: update_cache=yes
  when: apt_update_cache or backports_grep.rc != 0
- name: "apt: install fdroidserver dependencies"
  apt:
    install_recommends: no
    update_cache: no
    name:
      # python dependencies
      - androguard
      - default-jdk-headless
      - python3-args
      - python3-asn1crypto
      - python3-babel
      - python3-clint
      - python3-defusedxml
      - python3-git
      - python3-gitdb
      - python3-libcloud
      - python3-libvirt
      - python3-lockfile
      - python3-paramiko
      - python3-pyasn1-modules
      - python3-qrcode
      - python3-ruamel.yaml
      - python3-setuptools
      - python3-simplejson
      - python3-smmap
      - python3-vagrant
      - python3-yaml
      # fdroid cli dependencies
      - android-sdk
      - bzr
      - git-svn
      - mercurial
      - opensc
      - opensc-pkcs11
      - rsync
      - s3cmd
      - subversion
      - vagrant-mutate

- name: "apt: install fdroidserver dependencies for stretch/buster"
  apt:
    install_recommends: no
    update_cache: no
    name:
      - python-dateutil
      - python-magic
  when: ansible_distribution == 'Debian' and ansible_distribution_release in ['stretch', 'buster']

- name: "apt: install apksigner on releases that have it"
  apt:
    install_recommends: no
    update_cache: no
    name:
      - apksigner
  when: ansible_distribution == 'Debian' and ansible_distribution_release != 'stretch'
